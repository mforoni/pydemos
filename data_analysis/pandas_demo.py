from pprint import pformat

import pandas as pd

from helpers.helpers import configure_logger
from helpers.helpers import home_path

ZEROS_AND_NULLS = 'zeros & nulls'
NULLS = 'nulls'
ZEROS = 'zeros'

CSV_HEADER = ['Name', 'Age', 'Email']
CSV_FILE_PATH = f'{home_path()}/Workspaces/data/test.csv'

# pd.set_option('display.max_rows', 500)
# pd.set_option('display.max_columns', 500)
# pd.set_option('display.width', 1000)

logger = configure_logger()


def read_data(file_path, names, nrows=None):
    return pd.read_csv(file_path, names=names, nrows=nrows)


def zeros_and_nulls_dataframe(df):
    zeros = (df == 0.00).astype(int).sum(axis=0)
    nulls = df.isnull().sum()
    nulls_pct = 100 * df.isnull().sum() / len(df)
    mz_table = pd.concat([zeros, nulls, nulls_pct], axis=1)
    mz_table = mz_table.rename(
        columns={0: ZEROS, 1: NULLS, 2: '% of nulls'})
    mz_table[ZEROS_AND_NULLS] = mz_table[ZEROS] + mz_table[NULLS]
    mz_table['% of zeros and nulls'] = 100 * mz_table[ZEROS_AND_NULLS] / len(df)
    mz_table['data type'] = df.dtypes
    mz_table = mz_table[mz_table.iloc[:, 1] != 0].round(1)
    # print("Your selected dataframe has " + str(df.shape[1]) + " columns and " + str(df.shape[0]) + " Rows.")
    # print("There are " + str(mz_table.shape[0]) + " columns that have missing values.")
    return mz_table


def null_percentage_per_column(df):
    percent_missing = df.isnull().sum() * 100 / len(df)
    return pd.DataFrame({'column_name': df.columns, 'percent_missing': percent_missing})


def zero_percentage_per_column(df):
    zero_percentage = df[df == 0].count(axis=0) * 100 / len(df.index)
    return pd.DataFrame({'column_name': df.columns, 'percent_of_zero': zero_percentage})


def inspect_csv(file_path, header):
    df = pd.read_csv(file_path, names=header)
    logger.info(pformat(df))
    df_nulls = null_percentage_per_column(df)
    logger.info(pformat(df_nulls))
    logger.info(pformat(zero_percentage_per_column(df)))
    logger.info(pformat(zeros_and_nulls_dataframe(df)))


def main():
    inspect_csv(CSV_FILE_PATH, CSV_HEADER)


if __name__ == "__main__":
    main()
