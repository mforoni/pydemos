from pprint import pformat

import pandas as pd
import pandasql as ps

from helpers.helpers import home_path

CSV_HEADER = ['Name', 'Age', 'Email']
CSV_FILE_PATH = f'{home_path()}/Workspaces/data/test.csv'


def read_data():
    df = pd.read_csv(CSV_FILE_PATH, names=CSV_HEADER)
    print(pformat(df))
    sql = 'select Age from df'
    print('Executing sql query on df:', sql)
    df_sql = ps.sqldf(sql, locals())
    print(pformat(df_sql))


def main():
    read_data()


if __name__ == "__main__":
    main()
