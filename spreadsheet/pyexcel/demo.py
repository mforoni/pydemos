import pyexcel as pe

from helpers.helpers import configure_logger

logger = configure_logger()

FILE_PATH = '/media/marco/DATA/Repositories/gaming/tracking/3DS.ods'
SHEET_NAME = 'ITEMS'


def read_spreadsheet(file, sheet_name):
    logger.info('Reading spreadsheet %s', file)
    book = pe.get_book(file_name=file)
    logger.info('Sheets: %s', book.sheet_names())
    sheet = book.sheet_by_name(sheet_name)
    logger.info('Reading content of sheet %s', sheet_name)
    for row in sheet.rows():
        logger.info(row)
    logger.info('Reading content of sheet %s as records', sheet_name)
    records = pe.iget_records(file_name=file)
    for r in records:
        logger.info(r)
    pe.free_resources()


def main():
    read_spreadsheet(FILE_PATH, SHEET_NAME)


if __name__ == "__main__":
    main()
