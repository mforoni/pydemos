import datetime
import json
import pprint

import pyexcel
from pyexcel_ods3 import get_data

from helpers.helpers import configure_logger

logger = configure_logger()

FILE_PATH = '/media/marco/DATA/Repositories/gaming/tracking/3DS.ods'
SHEET_NAME = 'ITEMS'


def datetime_handler(x):
    if isinstance(x, datetime.datetime):
        return x.isoformat()
    elif isinstance(x, datetime.date):
        return x.isoformat()
    raise TypeError("Unknown type " + type(x))


def read_as_json(file):
    data = get_data(file)
    result = json.dumps(data, default=datetime_handler)
    logger.info(pprint.pformat(result, width=180))


def read_as_array(file):
    data = pyexcel.get_array(file_name=file)
    data = [[data[x][y].isoformat() if isinstance(data[x][y], datetime.date) else data[x][y] for y in
             range(len(data[x]))] for x in range(len(data)) if data[x][0]]
    logger.info(pprint.pformat(data, width=500))


def read_sheet(file, sheet_name):
    book = pyexcel.get_book(file_name=file)
    sheet = book.sheet_by_name(sheet_name)
    rows = sheet.rows()
    logger.info(rows)
    array = sheet.get_array()
    logger.info(pprint.pformat(array, width=500))


def main():
    # read_as_json(FILE_PATH)
    read_as_array(FILE_PATH)
    read_sheet(FILE_PATH, 'ITEMS')


if __name__ == "__main__":
    main()
