import logging
import pprint

import gspread
from oauth2client.service_account import ServiceAccountCredentials

from helpers.helpers import configure_logger

SPREADSHEET_NAME = 'games'

logger = configure_logger()


def read_gspread(filename):
    logger.info('Reading google spreadsheet %s', filename)
    scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
    credentials = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', scope)
    client = gspread.authorize(credentials)
    spreadsheet = client.open('games')
    logger.info('Worksheets: %s', spreadsheet.worksheets())
    for sheet in spreadsheet.worksheets():
        logger.info('Sheet %s contains %s rows', sheet.title, sheet.row_count)
        # sheet.row_count() always returns 1000?
        data = sheet.get_all_values()
        logging.info(pprint.pformat(data, width=260))


def main():
    read_gspread(SPREADSHEET_NAME)


if __name__ == "__main__":
    main()
