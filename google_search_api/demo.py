import pprint
import logging

from helpers.helpers import configure_logger
from google import google

logging.basicConfig(level=logging.INFO)
logger = configure_logger()


def search(text):
    results = google.search(text)
    for r in results:
        logger.info(pprint.pformat(r, width=180))


def main():
    search('Metal Gear Solid')


if __name__ == "__main__":
    main()
