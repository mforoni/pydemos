from pymongo import MongoClient

from helpers.helpers import configure_logger

logger = configure_logger()


def insert_one(db):
    posts = db.posts
    post_data = {
        'title': 'Python and MongoDB',
        'content': 'PyMongo is fun, you guys',
        'author': 'Scott'
    }
    result = posts.insert_one(post_data)
    logger.info('One post: %s', result.inserted_id)


def insert_many(db):
    posts = db.posts
    post_1 = {
        'title': 'Python and MongoDB',
        'content': 'PyMongo is fun, you guys',
        'author': 'Scott'
    }
    post_2 = {
        'title': 'Virtual Environments',
        'content': 'Use virtual environments, you guys',
        'author': 'Scott'
    }
    post_3 = {
        'title': 'Learning Python',
        'content': 'Learn Python, it is easy',
        'author': 'Bill'
    }
    results = posts.insert_many([post_1, post_2, post_3])
    logger.info('Multiple posts: %s', results.inserted_ids)


def find_one(db):
    posts = db.posts
    bills_post = posts.find_one({'author': 'Bill'})
    logger.info(bills_post)


def main():
    client = MongoClient()
    logger.info('Databases: %s', client.list_database_names())
    db = client.pymongo_test
    insert_one(db)
    insert_many(db)
    find_one(db)
    logger.info('All posts:')
    logger.info(db.posts)


if __name__ == "__main__":
    main()
