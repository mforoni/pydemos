# Python demos

Contains some code snippets and samples of various python libraries.

## Google Search API

[Google-Search-API](https://github.com/abenassi/Google-Search-API) is a python based library for searching various functionalities of google. It uses screen scraping to retrieve the results, and thus is unreliable if the way google's web pages are returned change in the future. 

See [google_search_api/demo.py](google_search_api/demo.py): a simple script useful for performing google search operations while controlling the results in an automated way.

```bash
$ python -m google_search_api.demo
```

## Pandas

TBC

Copy the file `test.csv` into `~\Workspaces\data` (or edit the path in the script).

### PandaSQL

TBC

## MongoDB

### PyMongo

[PyMongo](https://api.mongodb.com/python/current/) is a Python distribution containing tools for working with MongoDB, and is the recommended way to work with MongoDB from Python. Read more at [api.mongodb.com](https://api.mongodb.com/python/current/).

See [pymongo/demo.py](pymongo/demo.py): a simple script to test insertion/retrieval operations from a Document Store created following this [tutorial](https://realpython.com/introduction-to-mongodb-and-python/).

Before running the script the `mongod` service must be started:

```bash
sudo service mongod start
python -m pymongo.demo
```

## Manipulating spreadsheets

### pyexcel

[pyexcel](https://github.com/pyexcel/pyexcel): Single API for reading, manipulating and writing data in csv, ods, xls, xlsx and xlsm files. Read more at [docs.pyexcel.org](http://docs.pyexcel.org).

See [spreadsheet/pyexcel/demo.py](spreadsheet/pyexcel/demo.py): a simple script useful for reading the content of a spreadsheets.

### gspread

[Google Spreadsheets Python API v4](https://github.com/burnash/gspread): Simple interface for working with Google Sheets. Features:

* Google Sheets API v4.
* Open a spreadsheet by its title or url.
* Extract range, entire row or column values.
* Python 3 support.

Useful links:

* [Google Spreadsheets and Python](https://www.twilio.com/blog/2017/02/an-easy-way-to-read-and-write-to-a-google-spreadsheet-in-python.html) [article]
* [Google Sheets and Python](https://www.youtube.com/watch?v=vISRn5qFrkM) [video]

See [spreadsheet/gspread/demo.py](spreadsheet/gspread/demo.py): a simple script to read the content of your google drive spreadsheet.

## Parsing wiki text

### wptools

[Wikipedia tools (for Humans)](https://github.com/siznax/wptools): Python and command-line MediaWiki access for Humans

* get page extracts, image, Infobox data, Wikidata, and more
* get a random page, category, or site
* get page statistics
* get category members
* get site info and stats
* get data in any language

This package is intended to make it as easy as possible to get data from MediaWiki instances, expose more Wikidata, and extend Wikimedia APIs just for kicks. We say "(for Humans)" because that is a goal.

See [wikipedia/wptools/demo.py](wikipedia/wptools/demo.py): a simple script useful for retrieving data from Wikipedia pages.

### WikiTextParser

[WikiTextParser](https://github.com/5j9/wikitextparser): A simple to use WikiText parsing library for MediaWiki.
The purpose is to allow users easily extract and/or manipulate templates, template parameters, parser functions, tables, external links, wikilinks, lists, etc. found in wikitexts.

See [wikipedia/wikitextparser/demo.py](wikipedia/wikitextparser/demo.py): a simple script for extracting text from Wikipedia pages.

## SQLAlchemy

TBC

---

TODO:

* https://github.com/nithinmurali/pygsheets
