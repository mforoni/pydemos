import logging

from pathlib import Path


def configure_logger(level=logging.INFO, logfile_path='LogFile.log'):
    log_formatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
    root_logger = logging.getLogger()
    root_logger.setLevel(level)
    file_handler = logging.FileHandler(logfile_path)
    file_handler.setFormatter(log_formatter)
    root_logger.addHandler(file_handler)
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    root_logger.addHandler(console_handler)
    return root_logger


def home_path():
    return str(Path.home())
