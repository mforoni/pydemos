import mwparserfromhell
import requests
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

API_URL = "https://en.wikipedia.org/w/api.php"


def parse(title):
    params = {
        "action": "query",
        "prop": "revisions",
        "rvprop": "content",
        "rvslots": "main",
        "rvlimit": 1,
        "titles": title,
        "format": "json",
        "formatversion": "2",
    }
    headers = {"User-Agent": "My-Bot-Name/1.0"}
    req = requests.get(API_URL, headers=headers, params=params)
    res = req.json()
    revision = res["query"]["pages"][0]["revisions"][0]
    text = revision["slots"]["main"]["content"]
    return mwparserfromhell.parse(text)


def main():
    result = parse('Bayonetta')
    logger.info(result)


if __name__ == "__main__":
    main()
