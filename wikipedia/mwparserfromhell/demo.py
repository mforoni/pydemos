import mwparserfromhell as wikiparser
import wptools

from helpers.helpers import configure_logger

logger = configure_logger()

TITLES = [
    'Bayonetta',
    # "Assassin's Creed II"
]


def print_info(title):
    title = title.replace(' ', '_')
    logger.info("Retrieving Info box data for '%s'", title)
    page = wptools.page(title, silent=True)
    page.get_parse()
    info_box = page.data['infobox']  # info_box is a dict
    for k in info_box.keys():
        print_data(info_box, k)


def print_data(info_box, key):
    text = info_box[key]
    logger.info('Source - %s = %s', key, text)
    parsed = wikiparser.parse(text)
    logger.info('Parsed - %s = %s', key, parsed)


def main():
    for title in TITLES:
        print_info(title)


if __name__ == "__main__":
    main()
