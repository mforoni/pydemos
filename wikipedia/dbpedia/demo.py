import logging
import requests
import pprint

DATA_URL = 'http://dbpedia.org/data/{0}.json'
RESOURCE_URL = 'http://dbpedia.org/resource/'

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


def print_info(title):
    logger.info("Retrieving DBPedia data for '%s'", title)
    formatted_title = title.replace(" ", "_")
    data_url = DATA_URL.format(formatted_title)
    json = requests.get(data_url).json()
    data = json[RESOURCE_URL + formatted_title]  # dictionary
    pprint.pprint(data, width=180)


TITLES = ['Bayonetta', 'Super Mario 64', 'Metal Gear Solid', "Assassin's Creed II"]


def main():
    for title in TITLES:
        print_info(title)


if __name__ == "__main__":
    main()
