import logging
import wikipedia
import pprint

from collections import namedtuple

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


def search(keywords):
    return wikipedia.search(keywords)


def print_info(title):
    page = wikipedia.page(title)
    WikiPage = namedtuple('Wikipage', 'title url summary categories sections')
    wp = WikiPage(title=page.title, url=page.url, summary=page.summary, categories=page.categories,
                  sections=page.sections)
    pprint.pprint(wp._asdict(), width=180)


TITLES = ['Bayonetta', 'Super Mario 64', 'Metal Gear Solid', "Assassin's Creed II"]


def main():
    for title in TITLES:
        print_info(title)


if __name__ == "__main__":
    main()
