import logging
import wptools
import wikitextparser as wtp

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


def print_info(title):
    title = title.replace(' ', '_')
    logger.info("Retrieving Info box data for '%s'", title)
    page = wptools.page(title, silent=True)
    page.get_parse()
    info_box = page.data['infobox']  # info_box is a dict
    for k in info_box.keys():
        print_data(info_box, k)


def print_data(info_box, key):
    text = info_box[key]
    logger.info('Source - %s = %s', key, text)
    parsed = parse_wiki_text(text)
    logger.info('Parsed - %s = %s', key, parsed)


def parse_wiki_text(text):
    parsed = wtp.parse(text)
    for t in parsed.templates:
        logger.info('Template = %s', t)
    for l in parsed.lists():
        logger.info('List = %s', l)
    for wl in parsed.wikilinks:
        logger.info('wikilink = %s', wl)
    return parsed


TITLES = ['Bayonetta', "Assassin's Creed II"]


def main():
    for title in TITLES:
        print_info(title)


if __name__ == "__main__":
    main()
