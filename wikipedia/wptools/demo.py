import wptools
import pprint

from helpers.helpers import configure_logger

logger = configure_logger()


def print_info(title):
    title = title.replace(' ', '_')
    logger.info("Retrieving Info box data for '%s'", title)
    page = wptools.page(title, silent=True)
    page.get_parse()
    info_box = page.data['infobox']
    logger.info('infobox object has type %s', type(info_box))
    logger.info(pprint.pformat(info_box, width=180))


TITLES = ['Bayonetta', "Assassin's Creed II"]


def main():
    for title in TITLES:
        print_info(title)


if __name__ == "__main__":
    main()
