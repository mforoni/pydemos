import logging

from sqlalchemy.orm import sessionmaker

from sql_alchemy.cache import Platforms, Reviewers, PS3, GAME_SURF
from sql_alchemy.models import Base, engine

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


def main():
    Base.metadata.create_all()
    session = sessionmaker(bind=engine)()

    reviewers = Reviewers(session)
    reviewers.load()
    logger.info(reviewers.get(GAME_SURF))

    platforms = Platforms(session)
    platforms.load()
    logger.info(platforms.get(PS3))
    p = platforms.get('MISSING')
    logger.info(p)


if __name__ == "__main__":
    main()
