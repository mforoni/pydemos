import logging

from sqlalchemy.orm import sessionmaker

from sql_alchemy.models import Base, engine
from sql_alchemy.models import Platform, Reviewer, Review

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


def main():
    Base.metadata.create_all()
    session = sessionmaker(bind=engine)()
    for platform in session.query(Platform).all():
        logger.info(platform)
    for reviewer in session.query(Reviewer).all():
        logger.info(reviewer)
    for review in session.query(Review).all():
        logger.info(review)


if __name__ == "__main__":
    main()
