CREATE TABLE IF NOT EXISTS platform
(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL UNIQUE,
  short_name TEXT UNIQUE
);

CREATE TABLE IF NOT EXISTS series
(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS game
(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL,
  name_it TEXT,
  developers TEXT,
  publishers TEXT,
  directors TEXT,
  producers TEXT,
  artists TEXT,
  writers TEXT,
  composers TEXT,
  series_id INTEGER,
  genres TEXT,
  modes TEXT,
  FOREIGN KEY(series_id) REFERENCES series(id)
);


CREATE TABLE IF NOT EXISTS game_release
(
  game_id INTEGER,
  platform_id INTEGER,
  name TEXT,
  name_it TEXT,
  na_release_date INTEGER,
  eu_release_date INTEGER,
  au_release_date INTEGER,
  jp_release_date INTEGER,
  ww_release_date INTEGER,
  PRIMARY KEY (game_id, platform_id),
  FOREIGN KEY(game_id) REFERENCES game(id),
  FOREIGN KEY(platform_id) REFERENCES platform(id)
);

CREATE TABLE IF NOT EXISTS reviewer
(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL,
  url TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS review
(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  title TEXT NOT NULL,
  score REAL,
  date INTEGER NOT NULL,
  author TEXT NOT NULL,
  url TEXT NOT NULL UNIQUE,
  game_id INTEGER,
  platform_id INTEGER NOT NULL,
  reviewer_id INTEGER NOT NULL,
  graphics FLOAT,
  sound FLOAT,
  gameplay FLOAT,
  longevity FLOAT,
  item TEXT,
  FOREIGN KEY(game_id) REFERENCES game(id),
  FOREIGN KEY(platform_id) REFERENCES platform(id)
  FOREIGN KEY(reviewer_id) REFERENCES reviewer(id)
);

CREATE INDEX index_review_url ON review (url);