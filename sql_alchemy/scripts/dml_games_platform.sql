insert into platform (name, short_name) values ('PlayStation 4', 'PS4');
insert into platform (name, short_name) values ('PlayStation 3', 'PS3');
insert into platform (name, short_name) values ('PlayStation 2', 'PS2');
insert into platform (name, short_name) values ('PlayStation Portable', 'PSP');
insert into platform (name, short_name) values ('PlayStation', 'PS');
insert into platform (name, short_name) values ('PlayStation Vita', 'PS VITA');
insert into platform (name, short_name) values ('Nintendo Switch', 'Switch');
insert into platform (name, short_name) values ('Wii U', 'Wii U');
insert into platform (name, short_name) values ('Wii', 'Wii');
insert into platform (name, short_name) values ('GameCube', 'GC');
insert into platform (name, short_name) values ('Nintendo 64', 'N64');
insert into platform (name, short_name) values ('Super Nintendo Entertainment System', 'SNES');
insert into platform (name, short_name) values ('Nintendo Entertainment System', 'NES');
insert into platform (name, short_name) values ('Nintendo 3DS', '3DS');
insert into platform (name, short_name) values ('Nintendo DS', 'DS');
insert into platform (name, short_name) values ('Game Boy Advance', 'GBA');
insert into platform (name, short_name) values ('Game Boy Color ', 'GBC');
insert into platform (name, short_name) values ('Game Boy', 'Game Boy');
insert into platform (name, short_name) values ('Xbox One', 'XBO');
insert into platform (name, short_name) values ('Xbox 360', 'X360');
insert into platform (name, short_name) values ('Xbox', 'Xbox');
insert into platform (name, short_name) values ('Dreamcast', 'Dreamcast');
insert into platform (name, short_name) values ('Sega Saturn', 'Saturn');
insert into platform (name, short_name) values ('Microsoft Windows', 'Windows');
insert into platform (name, short_name) values ('iOS', 'iOS');
insert into platform (name, short_name) values ('Android', 'Android');
insert into platform (name, short_name) values ('Macintosh operating systems', 'Mac OS');


