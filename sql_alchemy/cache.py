import logging

from sql_alchemy.models import Platform, Reviewer, Review
from sql_alchemy.score import Score


# platforms short names
PS4 = 'PS4'
PS3 = 'PS3'
PS2 = 'PS2'
PSP = 'PSP'
PS = 'PS'
PS_VITA = 'PS VITA'
SWITCH = 'Switch'
WII_U = 'Wii U'
WII = 'Wii'
GC = 'GC'  # GameCube
N64 = 'N64'
SNES = 'SNES'
NES = 'NES'
N3DS = '3DS'
DS = 'DS'
GBA = 'GBA'  # Game Boy Advance
GBC = 'GBC'  # Game Boy Color
GAME_BOY = 'Game Boy'
XBOX_ONE = 'XBO'
XBOX_360 = 'X360'
XBOX = 'Xbox'
DREMCAST = 'Dreamcast'
SATURN = 'Saturn'
WINDOWS = 'Windows'
IOS = 'iOS'
ANDROID = 'Android'
N_GAGE = 'N-Gage'
MAC_OS = 'Mac OS'

# reviewer names
GAME_SURF = 'Game Surf'
EVERYEYE = 'everyeye.it'
IGN = 'IGN'
IGN_ITALIA = 'IGN Italia'
SPAZIOGAMES_IT = 'SPAZIOGAMES.IT'
GAMESPOT = 'GAMESPOT'

logging.basicConfig(level=logging.INFO)
log = logging.getLogger()


class Platforms:
    platforms = dict()

    def __init__(self, session):
        self.session = session

    def __repr__(self):
        return "<Platforms(session='%r', dict='%r')>" % (
            self.session, self.platforms)

    def load(self):
        for p in self.session.query(Platform).all():
            self.platforms[p.short_name.upper()] = p
            self.platforms[p.name.upper()] = p
        return self

    def get(self, name):
        if name.upper() == 'PC':
            return self.platforms[WINDOWS.upper()]
        if name.upper() == 'IPHONE' or name.upper() == 'IPAD':
            return self.platforms[IOS.upper()]
        if name.upper() == 'MAC' or name.upper() == 'MAC OS X':
            return self.platforms[MAC_OS.upper()]
        if name.upper() == 'VITA':
            return self.platforms[PS_VITA.upper()]
        if name.upper() == 'PSONE' or name.upper() == 'PSX':
            return self.platforms[PS.upper()]
        if name.upper() == 'NGAGE':
            return self.platforms[N_GAGE.upper()]
        if name.upper() == 'NINTENDO WII':
            return self.platforms[WII.upper()]
        return self.platforms.get(name.upper())

    def safe_get(self, name):
        p = self.get(name)
        assert (p is not None), "Cannot find platform from name %s" % name
        return p

    def dict(self):
        return self.platforms


class Reviewers:
    reviewers = dict()

    def __init__(self, session):
        self.session = session

    def __repr__(self):
        return "<Reviewers(session='%r', dict='%r')>" % (
            self.session, self.reviewers)

    def load(self):
        for r in self.session.query(Reviewer).all():
            self.reviewers[r.name] = r
        return self

    def get(self, name):
        return self.reviewers.get(name)


class Reviews:
    reviews = dict()  # review can be mapped by url
    saved = dict()  # new reviews added in transaction

    def __init__(self, session):
        self.session = session

    def __repr__(self):
        return "<Reviews(session='%r', dict='%r')>" % (
            self.session, self.reviews)

    def load(self):
        for r in self.session.query(Review).all():
            self.reviews[r.url] = r
        return self

    def get(self, url):
        return self.reviews.get(url)

    def save(self, r):
        log.info('Adding new review: %s', r)
        if r.score is None:
            log.warning('Missing score value')
        if self.saved.get(r.url) is None:
            self.saved[r.url] = r
            self.session.add(r)
        else:
            log.warning('Review already saved')

    def merge(self, stored, score, author, url, item=None):
        # TODO add title
        changed = False
        stored_score = Score(stored.score, stored.graphics, stored.sound, stored.gameplay, stored.longevity)
        if score != stored_score:
            stored.score = score.value
            stored.graphics = score.graphics
            stored.sound = score.sound
            stored.gameplay = score.gameplay
            stored.longevity = score.longevity
            changed = True
            log.info('Score changed from %r to %r', stored_score, score)
        if author != stored.author:
            stored.author = author
            changed = True
            log.info('Author changed from %s to %s', stored.author, author)
        if url != stored.url:
            stored.url = url
            changed = True
            log.info('URL changed from %s to %s', stored.url, url)
        if item is not None and item != stored.item:
            stored.item = item
            changed = True
            log.info('Item changed from %s to %s', stored.item, item)
        if changed:
            log.info('Updating review %r', stored)
            self.session.add(stored)
        else:
            log.info('No change detected')

    def commit(self):
        log.info('Commit')
        self.session.commit()
        for sr in self.saved.values():
            self.reviews[sr.url] = sr
        self.saved.clear()
