def str_to_float(s):
    return float(s.replace(',', '.')) if s is not None else None


def to_float(o):
    return str_to_float(o) if type(o) is str else o


class Score:

    def __init__(self, value=None, graphics=None, sound=None, gameplay=None, longevity=None):
        self.value = to_float(value)
        self.graphics = to_float(graphics)
        self.sound = to_float(sound)
        self.gameplay = to_float(gameplay)
        self.longevity = to_float(longevity)

    def __eq__(self, other):
        if isinstance(other, Score):
            return self.value == other.value and self.graphics == other.graphics and self.sound == other.sound \
                   and self.gameplay == other.gameplay and self.longevity == other.longevity
        return False

    def __repr__(self):
        return "%s [%s, %s, %s, %s]" % (self.value, self.graphics, self.sound, self.gameplay, self.longevity)

    def __str__(self):
        return self.value if self.value is not None else ''
