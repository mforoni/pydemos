from sqlalchemy import Column, Integer, String, Float
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('sqlite:///sqlite/games.db', echo=False)
Base = declarative_base(bind=engine)


class Platform(Base):
    __tablename__ = 'platform'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    short_name = Column(String)

    def __init__(self, name, short_name):
        self.name = name
        self.short_name = short_name

    def __repr__(self):
        return "<Platform(name='%s', short_name='%s')>" % (
            self.name, self.short_name)


class Reviewer(Base):
    __tablename__ = 'reviewer'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    url = Column(String)

    def __init__(self, name, url):
        self.name = name
        self.url = url

    def __repr__(self):
        return "<Reviewer(name='%s', url='%s')>" % (
            self.name, self.url)


class Review(Base):
    __tablename__ = 'review'
    id = Column(Integer, primary_key=True)
    title = Column(String)
    score = Column(Float, nullable=True)
    date = Column(Integer)
    author = Column(String)
    url = Column(String)
    game_id = Column(Integer)
    platform_id = Column(Integer)
    reviewer_id = Column(Integer)
    graphics = Column(Float, nullable=True)
    sound = Column(Float, nullable=True)
    gameplay = Column(Float, nullable=True)
    longevity = Column(Float, nullable=True)
    item = Column(String, nullable=True)

    def __init__(self, title, score, date, author, url, game_id, platform_id, reviewer_id, graphics=None, sound=None,
                 gameplay=None, longevity=None, item=None):
        self.title = title
        self.score = score
        self.date = date
        self.author = author
        self.url = url
        self.game_id = game_id
        self.platform_id = platform_id
        self.reviewer_id = reviewer_id
        self.graphics = graphics
        self.sound = sound
        self.gameplay = gameplay
        self.longevity = longevity
        self.item = item

    def __repr__(self):
        return "<Review(title='%s', score='%r', date='%s', author='%s', url='%s', game_id='%s', platform_id='%s'," \
               " reviewer_id='%s', graphics='%r', sound='%r', gameplay='%r', longevity='%r', item='%s')>" % (
                   self.title, self.score, self.date, self.author, self.url, self.game_id, self.platform_id,
                   self.reviewer_id, self.graphics, self.sound, self.gameplay, self.longevity, self.item)


class Game(Base):
    __tablename__ = 'game'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    name_it = Column(String, nullable=True)
    developers = Column(String, nullable=True)
    publishers = Column(String, nullable=True)
    directors = Column(String, nullable=True)
    producers = Column(String, nullable=True)
    designers = Column(String, nullable=True)
    artists = Column(String, nullable=True)
    writers = Column(String, nullable=True)
    composers = Column(String, nullable=True)
    series_id = Column(Integer, nullable=True)
    genres = Column(String, nullable=True)
    modes = Column(String, nullable=True)

    def __init__(self, name, name_it=None, developers=None, publishers=None, directors=None, producers=None,
                 designers=None, artists=None, writers=None, composers=None, series_id=None, genres=None, modes=None):
        self.name = name
        self.name_it = name_it
        self.developers = developers
        self.publishers = publishers
        self.directors = directors
        self.producers = producers
        self.designers = designers
        self.artists = artists
        self.writers = writers
        self.composers = composers
        self.series_id = series_id
        self.genres = genres
        self.modes = modes

    def __repr__(self):
        return "<Game(name='%s', name_it='%s', developers='%s', publishers='%s', directors='%s', producers='%s'," \
               "designers='%s', artists='%s', writers='%s', composers='%s', series_id='%r', genres='%s', modes='%s')>" \
               % (self.name, self.name_it, self.developers, self.publishers, self.directors, self.producers,
                  self.designers, self.artists, self.writers, self.composers, self.series_id, self.genres, self.modes)


class GameRelease(Base):
    __tablename__ = 'game_release'
    game_id = Column(Integer, primary_key=True)
    platform_id = Column(Integer, primary_key=True)
    name = Column(String, nullable=True)
    name_it = Column(String, nullable=True)

    def __init__(self, game_id, platform_id, name=None, name_it=None):
        self.game_id = game_id
        self.platform_id = platform_id
        self.name = name
        self.name_it = name_it

    def __repr__(self):
        return "<GameRelease(game_id='%d', platform_id='%d', name='%s', name_it='%s')>" % (
            self.game_id, self.platform_id, self.name, self.name_it)
