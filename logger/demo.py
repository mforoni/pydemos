# import sys
# print(sys.path)

from helpers.helpers import configure_logger

logger = configure_logger()


def main():
    logger.debug('debug message')
    logger.info('info message')
    logger.warning('warning message')
    logger.error('error message')
    logger.critical('critical message')


if __name__ == "__main__":
    main()
